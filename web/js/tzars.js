/**
 * Created by ganbarov_emin on 04/04/15.
 */
$(document).ready(function(){
    var dialogNumber,
        imgPath,
        beginNumberPhoto,
        endNumberPhoto,
        blockTzars,
        htmlBlock,
        bodyBlock,
        overlay,
        timeChangeDialog = 1000,
        timeLastSlide = 11000;

    blockTzars = $('#dialog_governments');
    overlay = $('#overlay');
    dialogNumber = Math.floor((Math.random() * 7) + 1);
    imgPath = '/images/dialogs/0' + dialogNumber + '/sprite.png';
    htmlBlock = $('html');
    bodyBlock = $('body');
    beginNumberPhoto = 1;
    endNumberPhoto = 4;

    //Отрабатывает только на странице диалогов царей!
    if (!blockTzars.length) {
        return false;
    }

    htmlBlock.css('background-color', 'rgb(218, 75, 44)');
    bodyBlock.css('background-color', 'rgb(218, 75, 44)');

    $(blockTzars).css('backgroundImage', 'url(' + imgPath + ')');
    changeDialog(beginNumberPhoto);

    /**
     * Меняет картинки диалога
     * @param number
     * @returns {boolean}
     */
    function changeDialog(number)
    {
        var position;
        switch (number) {
            case 1:
                position = 0;
                break;
            case 2:
                position = 640;
                break;
            case 3:
                position = 1280;
                break;
            case 4:
                position = 1920;
                break;
        }

        if (number <= endNumberPhoto) {
            setTimeout(function(){
                $(blockTzars).css('backgroundPosition', 'left -' + position + 'px');
                changeDialog(number + 1);
            }, timeChangeDialog);
        } else {
            /*setTimeout(function(){
                htmlBlock.css('background-color', 'transparent');
                bodyBlock.css('background-color', 'transparent');
                blockTzars.css('display', 'none');
                location.href = "http://tzars.ru/tzars/all";
            }, timeLastSlide);*/
            return false;
        }
    }

    /**
     * Отображем диалоги правителей
     */
    function showBubble()
    {
        var prevBubble;
        $('.government').on('click', function(){
            var curTzar = $(this),
                curBubble,
                faceName = $(curTzar).attr('id'),
                faceId = faceName.slice((faceName.length - 2), faceName.length);

            //Скрываем предыдущий бабл
            if (prevBubble) {
                $(prevBubble).css('display', 'none');
                prevBubble = '#bubble_' + faceId;
            } else {
                prevBubble = '#bubble_' + faceId;
            }

            //Показываем бабл
            curBubble = $(curTzar).find('#bubble_' + faceId);
            if (curBubble.attr('data-show') == 0) {
                curBubble.attr('data-show', 1);
                curBubble.css('display', 'block');
                curTzar.css('zIndex', 2);
                overlay.css('display', 'block');
            } else {
                curBubble.attr('data-show', 0);
                curBubble.css('display', 'none');
                overlay.css('display', 'none');
                curTzar.css('zIndex', 0);
            }

            //По клику на оверлэй скрывает его и бабл
            $(overlay).on('click', function(){
                overlay.css('display', 'none');
                curBubble.css('display', 'none');
                curBubble.css('data-show', 0);
                curTzar.css('zIndex', 0);
            });
        });
    }
});
