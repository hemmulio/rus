$('#palitra').on('click', '#send_to_cart', function () {
	var posterBlock = $('div').is('#poster_param');
	if (!posterBlock) {
		var color = $('input[name=color]:checked').val();
		var cut = $('input[name=cut]:checked').val();
		var size = $('input[name=size]:checked').val();
		var count = $('#count').val();
		var is_update = location.pathname == '/cart/update';
		var selected_id = $('#send_to_cart').data('selected_id');
		var url = selected_id ? '/cart/add?id=' + selected_id : '/cart/add';

		$.ajax({
			url: url,
			type: 'POST',
			data: {
				color: color,
				cut: cut,
				size: size,
				count: count
			},
			beforeSend: function () {
				if (!color) {
					$('#error').html('Пожалуйста, выберите цвет.').addClass("error");
					return false;
				} else if (!size) {
					$('#error').html('Пожалуйста, выберите размер.').addClass("error");
					return false;
				} else if (!cut) {
					$('#error').html('Пожалуйста, выберите крой.').addClass("error");
					return false;
				} else {
					$('#error').html('').removeClass("error");
					return true;
				}
			},
			// прнимаю ответ
			success: function (data) {
				if (is_update) {
					document.location = '/cart/cart';
				}
				var count = parseInt(data, 10);
				console.log(count);

				if (count) {
					var current_count = $('#cart_count').data('count'); // смотрю сколько сейчас в корзине
					$('#cart_count').data('count', parseInt(current_count, 10) + count); // записываю новое значение в data
					$('#cart_count').html($('#cart_count').data('count')); // показываю визуально
					$('#cart-img').addClass('full');

					$('.hide_on_empty').removeClass('empty');
				}
			}
		})
	} else {
		var border = $('input[name=border]:checked').val();
		count = $('#count').val();
		selected_id = $('#send_to_cart').data('selected_id');
		url = selected_id ? '/poster/add?id=' + selected_id : '/poster/add';
		if (border == 2) {
			cut = 1;
		} else {
			cut = border;
		}

		$.ajax({
			url: url,
			type: 'POST',
			data: {
				border: border,
				cut: cut,
				count: count
			},
			beforeSend: function () {
				if (!border) {
					$('#error').html('Пожалуйста, выберите рамку.').addClass("error");
					return false;
				} else if (!cut) {
					$('#error').html('Пожалуйста, выберите рамку.').addClass("error");
					return false;
				} else {
					$('#error').html('').removeClass("error");
					return true;
				}
			},
			//Получаем ответ
			success: function (data) {
				/*if (is_update) {
					document.location = '/cart/cart';
				}*/
				var count = parseInt(data, 10);
				if (count) {
					var current_count = $('#cart_count').data('count'); // смотрю сколько сейчас в корзине
					$('#cart_count').data('count', parseInt(current_count, 10) + count); // записываю новое значение в data
					$('#cart_count').html($('#cart_count').data('count')); // показываю визуально
					$('#cart-img').addClass('full');

					$('.hide_on_empty').removeClass('empty');
				}
			}
		})
	}
});
