$(document).ready(function () {
	// var color = $('input[name=color]:checked').val();
	// var cut = $('input[name=cut]:checked').val();

	// $('#close').attr('src', '/images/small/' + colorsAvailable[color] + '.jpg');
	// $('#couple').attr('src', '/images/couple/' + cutsAvailable[cut] +'_'+ colorsAvailable[color] + '.jpg');
	// $('#t-short-image').attr('src', '/images/t-shirts/' + cutsAvailable[cut] +'_'+ colorsAvailable[color] + '.jpg');
	// $('#t-short-image-big').attr('src', '/images/t-shirts/' + cutsAvailable[cut] +'_'+ colorsAvailable[color] + '.jpg');
	$('#palitra').on('click', 'input[name=color], input[name=cut]', function () {
		//скопировать из модели
		var colorsAvailable = ['azure','black','blue','DarkGreen','gray','green',
			'limon','orange','red','sky','yellow','white'];
		var cutsAvailable = ['M', 'W'];

		var color = $('input[name=color]:checked').val();
		var cut = $('input[name=cut]:checked').val();
		// console.log(cut);
		var cut_tmp = 0;
		var color_tmp = 1;
		if (cut == undefined && color != undefined) {
			if (color == 4 || color == 5 || color == 6 || color == 9) {
				cut_tmp = 0;
			};
			if (color == 0 || color == 3) {
				cut_tmp = 1;
			};
			$('#close').attr('src', '/images/small/' + colorsAvailable[color] + '.jpg');
			$('#couple').attr('src', '/images/couple/' + cutsAvailable[cut_tmp] +'_'+ colorsAvailable[color] + '.jpg');
			$('#t-short-image').attr('src', '/images/t-shirts/' + cutsAvailable[cut_tmp] +'_'+ colorsAvailable[color] + '.jpg');
			$('#t-short-image-big').attr('src', '/images/t-shirts/' + cutsAvailable[cut_tmp] +'_'+ colorsAvailable[color] + '.jpg');
		};
		if (cut != undefined && color == undefined) {
			if (cut == 0) {
				color_tmp = 1;
			};
			if (cut == 1) {
				color_tmp = 1;
			};

			$('#close').attr('src', '/images/small/' + colorsAvailable[color_tmp] + '.jpg');
			$('#couple').attr('src', '/images/couple/' + cutsAvailable[cut] +'_'+ colorsAvailable[color_tmp] + '.jpg');
			$('#t-short-image').attr('src', '/images/t-shirts/' + cutsAvailable[cut] +'_'+ colorsAvailable[color_tmp] + '.jpg');
			$('#t-short-image-big').attr('src', '/images/t-shirts/' + cutsAvailable[cut] +'_'+ colorsAvailable[color_tmp] + '.jpg');
		};
		//эта часть работает как надо, выбирается то
		if (cut == undefined  && color == undefined) {
			$('#close').attr('src', '/images/small/' + colorsAvailable[1] + '.jpg');
			$('#couple').attr('src', '/images/couple/' + cutsAvailable[0] +'_'+ colorsAvailable[1] + '.jpg');
			$('#t-short-image').attr('src', '/images/t-shirts/' + cutsAvailable[0] +'_'+ colorsAvailable[1] + '.jpg');
			$('#t-short-image-big').attr('src', '/images/t-shirts/' + cutsAvailable[0] +'_'+ colorsAvailable[1] + '.jpg');
		};
		//эта часть работает как надо, нельзя выбрать не то
		if (cut !== undefined  && color !== undefined) {
			$('#close').attr('src', '/images/small/' + colorsAvailable[color] + '.jpg');
			$('#couple').attr('src', '/images/couple/' + cutsAvailable[cut] +'_'+ colorsAvailable[color] + '.jpg');
			$('#t-short-image').attr('src', '/images/t-shirts/' + cutsAvailable[cut] +'_'+ colorsAvailable[color] + '.jpg');
			$('#t-short-image-big').attr('src', '/images/t-shirts/' + cutsAvailable[cut] +'_'+ colorsAvailable[color] + '.jpg');
		};

	});

	$('.subrow_img').click( function () {
		$('#t-short-image-big').attr('src', $(this).attr('src'));
	});

});

