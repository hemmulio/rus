$(document).ready(function () {
	var posterBlock = $('div').is('#poster_param');

	// позволяет выбрать тоьлко один из чекбоксов
	$('#palitra').on('click', "input:checkbox", function () {
		// переключалка по группе
		if (!posterBlock) {
			var box = $(this);
			if (box.is(":checked")) {
				var group = "input:checkbox[name='" + box.attr("name") + "']";
				$(group).prop("checked", false);
				box.prop("checked", true);
			} else {
				box.prop("checked", false);
			}

			var color = $('input[name=color]:checked').val();
			var cut = $('input[name=cut]:checked').val();
			var size = $('input[name=size]:checked').val();
			var count = $('#count').val();

			$.ajax({
				type: 'POST',
				url: '/cart/availibles',
				data: {
					color: color,
					cut: cut,
					size: size,
					count: count
				},
				// прнимаю ответ
				success: function (data) {
					// console.log(data);
					$('#palitra').html(data);


				}
			})
		} else {
			var box = $(this);
			if (box.is(":checked")) {
				//Снимаем все чекбоксы
				var group = "input:checkbox[name='" + box.attr("name") + "']";
				$(group).prop("checked", false);
				$(group).removeAttr('checked');
				$('.wrap-checkbox').removeClass('checked');
				//Устанавливаем текущий чекбокс
				box.prop("checked", true);
				box.parent().addClass('checked');
				//Меняем картинки в каруселе
				if (box.val() == 0) {
					$('#t-short-image-big').attr('src', '/images/posters/transparent_border_01.jpg');
					$('#t-short-image').attr('src', '/images/posters/transparent_border_01.jpg');
					$('#couple').attr('src', '/images/posters/transparent_border_02.jpg');
					$('#close').attr('src', '/images/posters/transparent_border_03.jpg');
					$('#end').attr('src', '/images/posters/transparent_border_04.jpg');
					//Меняем цену
					$('#cost_poster').html('2900.-');
				} else if (box.val() == 1) {
					$('#t-short-image-big').attr('src', '/images/posters/brown_border_01.jpg');
					$('#t-short-image').attr('src', '/images/posters/brown_border_01.jpg');
					$('#couple').attr('src', '/images/posters/brown_border_02.jpg');
					$('#close').attr('src', '/images/posters/brown_border_03.jpg');
					$('#end').attr('src', '/images/posters/brown_border_04.jpg');
					//Меняем цену
					$('#cost_poster').html('5500.-');
				} else if (box.val() == 2) {
					$('#t-short-image-big').attr('src', '/images/posters/silver_border_01.jpg');
					$('#t-short-image').attr('src', '/images/posters/silver_border_01.jpg');
					$('#couple').attr('src', '/images/posters/silver_border_02.jpg');
					$('#close').attr('src', '/images/posters/silver_border_03.jpg');
					$('#end').attr('src', '/images/posters/silver_border_04.jpg');
					//Меняем цену
					$('#cost_poster').html('5200.-');
				}
			}
		}
	});


	$('#palitra').on('change', '.wrap-color', function () {
		$('.wrap-color.checked').not(this).toggleClass('checked');
		$(this).toggleClass('checked'); /* переключатель класса .active */
	});


	$('#palitra').on('click', '.minus', function () {
		var input = $('#count');
		var count = parseInt(input.val(), 10) - 1;
		count = count < 1 ? 1 : count;
		input.val(count);
		// input.change();
		// return false;
	});


	$('#palitra').on('click', '.plus', function () {
		var input = $('#count');
		var count = parseInt(input.val(), 10) + 1;
		count = count < 1 ? 1 : count;
		input.val(count);
	});
})
