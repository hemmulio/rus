<?php
date_default_timezone_set("Europe/Moscow");

// comment out the following two lines when deployed to production
defined('YII_DEBUG') or define('YII_DEBUG', true);
defined('YII_ENV') or define('YII_ENV', 'dev');

require(__DIR__ . '/../vendor/autoload.php');
require(__DIR__ . '/../vendor/yiisoft/yii2/Yii.php');

// if (YII_DEBUG) {
    ini_set('display_errors', true);
    error_reporting(E_ALL & ~E_NOTICE & ~E_STRICT & ~E_DEPRECATED);
    // defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL', 3);
    // $yii = dirname(__FILE__) . '/framework/yii.php';

// } else {
//     ini_set('display_errors', FALSE);
//     error_reporting(FALSE);
    // $yii = dirname(__FILE__) . '/framework/yiilite.php';
// }
//

$config = require(__DIR__ . '/../config/web.php');

(new yii\web\Application($config))->run();
