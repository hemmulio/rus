<?php

namespace app\models;

use Yii;
use app\models\Product;

/**
 * This is the model class for table "order".
 *
 * @property integer $id
 * @property string $datatime
 * @property integer $status
 * @property string $name
 * @property string $phone
 * @property string $email
 * @property string $country
 * @property string $region
 * @property string $city
 * @property string $address
 * @property integer $mode_delivery
 * @property integer $mode_payment
 * @property string $admin_comment
 * @property string $user_comment
 *
 * @property Cart[] $carts
 */
class Order extends \yii\db\ActiveRecord
{

	/**
	 * Способ доставки
	 * @var array
	 */
	public static $mode_deliveryAvailable = ['Доставка курьером','Самовывоз', 'Почтой России'];

	/**
	 * Способ оплаты
	 * @var array
	 */
	public static $mode_paymentAvailable = [
		0=>'Оплата наличными при получении',
		1=>'Яндекс.Деньги',
		2=>'Банковская карта',
		3=>'Через терминал',
		4=>'Альфа-Клик',
	];


	/**
	 * Статус заказа
	 * @var array
	 */
	public static $status = [
		0 => 'Новый',
		1 => 'В обработке' ,
		2 => 'Оплачен',
		3 => 'Доставлен',
		4 => 'Отклонен',
		5 => 'В доставке'
	];

	public static $statusesRus = [
		0 => 'Новый',
		1 => 'В обработке' ,
		2 => 'Оплачен',
		3 => 'Доставлен',
		4 => 'Отклонен',
		5 => 'В доставке',
	];

	public static function getPaymentModeText($mode)
	{
		return [
			1=>'PC',
			2=>'AC',
			3=>'GP',
			4=>'AB',
		][$mode];
	}

	/**
	 * Название таблицы
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'order';
	}

	/**
	 * Правила валидации
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			// [['name', 'phone', 'email', 'country', 'region', 'city', 'address', 'mode_delivery', 'mode_payment'], 'required'],
			['name', 'required', 'message' => 'Пожалуйста, введите ваше имя'],
			['email', 'required', 'message' => 'Пожалуйста, введите ваш email'],
			['email', 'email', 'message' => 'введеное значение не является email адресом'],
			['phone', 'required', 'message' => 'Пожалуйста, введите ваш номер телефона'],
			//Ориентировано на российские мобильные + городские с кодом из 3 цифр (например, Москва).
			['phone', 'match', 'pattern' => '/^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$/', 'message' => 'телефон введен некорректно'],

			['address', 'required', 'message' => 'Пожалуйста, введите ваш адрес', 'when' => function($model) {
				return $model->mode_delivery == self::$mode_deliveryAvailable[0];
			}, 'whenClient' => "function (attribute, value) {
				return $('#order-mode_delivery').val() == 0;
			}"],


			[['datatime'], 'safe'],

			[['status', 'mode_delivery', 'mode_payment'], 'integer'],
			[['admin_comment', 'user_comment'], 'string'],
			[['name', 'phone', 'email', 'country', 'region', 'city', 'address'], 'string', 'max' => 255],
			//Ориентировано на российские мобильные + городские с кодом из 3 цифр (например, Москва).

		];
	}

	/**
	 * Атрибуты
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'datatime' => 'Время',
			'status' => 'Статус',
			'name' => 'Имя',
			'phone' => 'Телефон',
			'email' => 'E-mail',
			'address' => 'Адрес доставки',
			'mode_delivery' => 'Способ доставки',
			'mode_payment' => 'Способ оплаты',
			'admin_comment' => 'Комментарий',
		];
	}

	/**
	 * Получить карточки товара данного заказа
	 * @return \yii\db\ActiveQuery
	 */
	public function getCarts()
	{
		return $this->hasMany(Cart::className(), ['order_id' => 'id']);
	}

	/**
	 * Получить название статуса заказа
	 * @return string
	 */
	public function getStatusText()
	{
		return static::$statusesRus[$this->status];
	}

	/**
	 * Получить способ доставки
	 * @param $mode_delivery
	 * @return string
	 */
	public function getMode_deliveryValue($mode_delivery)
	{
		if(isset(self::$mode_deliveryAvailable[$mode_delivery])){
			return self::$mode_deliveryAvailable[$mode_delivery];
		}else{
			return '-';
		}
	}

	/**
	 * Получить статус заказа
	 * @param $status
	 * @return string
	 */
	public function getStatus($status)
	{
		if(isset(self::$status[$status])){
			return self::$status[$status];
		}else{
			return '-';
		}
	}

	/**
	 * Получить текст способа доставки
	 * @return string
	 */
	public function getMode_deliveryText()
	{
		if(isset(self::$mode_deliveryAvailable[$this->mode_delivery])){
			return self::$mode_deliveryAvailable[$this->mode_delivery];
		}else{
			return '-';
		}
	}

	/**
	 * Получить способ оплаты
	 * @param $mode_payment
	 * @return string
	 */
	public function getMode_paymentValue($mode_payment)
	{
		if(isset(self::$mode_paymentAvailable[$mode_payment])){
			return self::$mode_paymentAvailable[$mode_payment];
		}else{
			return 'Способ оплаты';
		}
	}

	/**
	 * Получить текст способа оплаты
	 * @return string
	 */
	public function getModePaymentText()
	{
		$result = '-';
		if (!empty(static::$mode_paymentAvailable[$this->mode_payment])) {
			$result = static::$mode_paymentAvailable[$this->mode_payment];
		}

		return $result;
	}

	/**
	 * Записываем дату после вставки записи
	 * @param bool $insert
	 * @return bool
	 */
	public function beforeSave($insert)
	{
		if (parent::beforeSave($insert)) {
			$this->datatime = date('Y-m-d H:i:s');
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Получить стоимость заказа
	 * @return int|mixed
	 */
	public function getCost()
	{
		$cost = 0;
		foreach ($this->carts as $cart) {
			$cost += $cart->product->price * $cart->count;
		}

		return $cost;
	}

	/**
	 * Получить описание заказа
	 * @return string
	 */
	public function getCargo()
	{
		$cost = '';
		foreach ($this->carts as $cart) {
			if ($cart->product->type == 0) {
				$cost .= 'Футболка ';
				$cost .= $cart->product->getCutText() . ', ';
				$cost .= $cart->product->getColorRu() . ', ';
				$cost .= $cart->product->getSizeText() . ', ';
				$cost .= $cart->count . ' шт. ';
			} elseif ($cart->product->type == 1) {
				$cost .= 'Плакат ';
				$cost .= Product::$posterTypeRus[$cart->product->cut] . ' ';
				$cost .= ($cart->product->color == 0) ? '' : Product::$posterColorRus[$cart->product->color] . ', ';
				$cost .= $cart->count . ' шт. ';
			}
		}

		return $cost;
	}

	/**
	 * Отправляем письмо пользователю, который сделал заказа на сайте
	 */
	public function sendHello()
	{
		Yii::$app->mailer->compose('@app/mail/hello', ['model'=>$this])
			->setFrom('info@tzars.ru')
			->setTo($this->email)
			->setSubject('Заказ на tzars.ru')
			->send();
	}


	/**
	 * Отправляем письмо с заказом в магазин
	 */
	public function sendAlarm()
	{
		Yii::$app->mailer->compose('@app/mail/alarm', ['model'=>$this])
			->setFrom('admin@tzars.ru')
			->setTo('info@tzars.ru')
			->setSubject('Заказ #'.$this->id)
			->send();
	}


}

