<?php

namespace app\models;

use Yii;
use yz\shoppingcart\CartPositionInterface;
use yz\shoppingcart\CartPositionTrait;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "product".
 *
 * @property   integer   $id		    ID записи
 * @property   string    $description   Описание
 * @property   string    $price			Цена
 * @property   integer   $cut			Крой
 * @property   string    $color			Цвет
 * @property   integer   $size			Размер
 * @property   integer   $available		Доступность
 * @property   integer   $type			Тип (футболка или плакат)
 *
 * @property Cart[] $carts
 */
class Product extends ActiveRecord implements CartPositionInterface
{
	use CartPositionTrait;

	/**
	 *  Крой футболки
	 * @var array
	 */
	public static $cutsAvailable = [
		0 => 'мужская',
		1 => 'женская'
	];

	/**
	 * Доступные цвета (для мужских и женских футболок)
	 * @var array
	 */
	public static $colorsAvailable = [
		0 => 'azure',
		1 => 'black',
		2 => 'blue',
		3 => 'DarkGreen',
		4 => 'gray',
		5 => 'green',
		6 => 'limon',
		7 => 'orange',
		8 => 'red',
		9 => 'sky',
		10 => 'yellow',
		11 => 'white',
	];

	/**
	 * Русские названия цветов
	 * @var array
	 */
	public static $colorsRu = [
		0 =>'голубой',
		1 =>'черный',
		2 =>'синий',
		3 =>'темно зеленый',
		4 =>'серый',
		5 =>'салатовый',
		6 =>'светло желтый',
		7 =>'оранжевый',
		8 =>'красный',
		9 =>'небесный',
		10 =>'желтый',
		11 =>'белый',
	];

	/**
	 * Доступные размеры футболок
	 * @var array
	 */
	public static $sizesAvailable = [
		0 => 'S',
		1 => 'XL',
		2 => 'M',
		3 => 'XXL',
		4 => 'L',
		5 => 'XXXL',
	];

	/**
	 * Тип товара (футболка или плакат)
	 * @var array
	 */
	public static $productType = [
		0 => 't_shirt',
		1 => 'poster'
	];

	/**
	 * Тип плаката (с рамкой и без рамки в базе это поле 'cut')
	 * @var array
	 */
	public static $posterType = [
		0 => 'without_border',
		1 => 'with_border'
	];

	/**
	 * Название типа плаката на русском
	 * @var array
	 */
	public static $posterTypeRus = [
		0 => 'без рамки',
		1 => 'с рамкой',
	];

	/**
	 * Цвет плаката
	 * @var array
	 */
	public static $posterColor = [
		0 => 'transparent',
		1 => 'brown',
		2 => 'silver',
	];

	/**
	 * Цвет плаката на русском
	 * @var array
	 */
	public static $posterColorRus = [
		0 => 'нет',
		1 => 'коричневый',
		2 => 'серебристый',
	];

	/**
	 * Получить цену продукта
	 * @return string
	 */
	public function getPrice()
	{
		return $this->price;
	}

	/**
	 * Получить ID записи
	 * @return int
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * Получить название таблицы
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'product';
	}

	/**
	 * Правила валидации
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['description', 'price', 'cut', 'color', 'size', 'available'], 'required'],
			[['description'], 'string'],
			[['price'], 'number'],
			[['cut'], 'in', 'range'=>array_keys(self::$cutsAvailable)],
			[['color'], 'in', 'range'=>array_keys(self::$colorsAvailable)],
			[['size'], 'in', 'range'=>array_keys(self::$sizesAvailable)],
			[['color'], 'string', 'max' => 255]
		];
	}

	/**
	 * Атрибуты к записям в таблице
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'description' => 'Описание',
			'price' => 'Цена',
			'cut' => 'Крой',
			'color' => 'Цвет',
			'size' => 'Размер',
			'available' => 'Доступен',
		];
	}

	/**
	 * Получить название цвета на русском языке
	 * @return string
	 */
	public function getColorRu()
	{
		if (isset(self::$colorsRu[$this->color])) {
			return self::$colorsRu[$this->color];
		} else {
			return $this->color;
		}
	}

	/**
	 * Получить размер
	 * @return int
	 */
	public function getSizeText()
	{
		if (isset(self::$sizesAvailable[$this->size])) {
			return self::$sizesAvailable[$this->size];
		} else {
			return $this->size;
		}
	}

	/**
	 * Получить крой
	 * @return int
	 */
	public function getCutText()
	{
		if (isset(self::$cutsAvailable[$this->cut])) {
			return self::$cutsAvailable[$this->cut];
		} else {
			return $this->size;
		}
	}

	/**
	 * Получить крой, если он есть, а если нет, то получить размер, как я понял, что имел ввиду автор
	 * @return int
	 */
	public function getavailableText()
	{
		if (isset(self::$cutsAvailable[$this->cut])) {
			return self::$cutsAvailable[$this->cut];
		} else {
			return $this->size;
		}
	}


	/**
	 * Получить продукт по параметрам
	 * @param   string   $selcut     выбранный крой
	 * @param   string   $selcolor   выбранный цвет
	 * @param   string   $selsize	 выбранный размер
	 * @return mixed
	 */
	public static function getAvailibles($selcut, $selcolor, $selsize)
	{
		// отдать массив с теми инпутами, которые изначально неактинвый
		$availables = [];
		foreach (self::$cutsAvailable as $key => $cut) {
			$where = [
				'cut' => $key,
				'available' => 1,
				'type' => 0,
			];

			if (is_numeric($selcolor) && $selcolor >= 0) {
				$where['color'] = $selcolor;
			}
			if (is_numeric($selsize) && $selsize >= 0) {
				$where['size'] = $selsize;
			}

			$availables['cut'][$key] = self::find()->where($where)->exists();
		}

		foreach (self::$colorsAvailable as $key => $color) {
			$where = [
				'color' => $key,
				'available' => 1,
				'type' => 0,
			];

			if (is_numeric($selcut) && $selcut >= 0) {
				$where['cut'] = $selcut;
			}

			if (is_numeric($selsize) && $selsize >= 0) {
				$where['size'] = $selsize;
			}
			$availables['color'][$key] = self::find()->where($where)->exists();
		}

		foreach (self::$sizesAvailable as $key => $size) {
			$where = [
				'size' => $key,
				'available' => 1,
				'type' => 0,
			];

			if (is_numeric($selcut) && $selcut >= 0) {
				$where['cut'] = $selcut;
			}
			if (is_numeric($selcolor) && $selcolor >= 0) {
				$where['color'] = $selcolor;
			}

			$availables['size'][$key] = self::find()->where($where)->exists();
		}

		return $availables;
	}


	/**
	 * Не понимаю, что хотел получить автор
	 */
	public function getvailableText()
	{
//		return ['Нет', 'Да'][$this->available];
	}
}
