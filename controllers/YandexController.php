<?php

namespace app\controllers;

use Yii;
use app\components\Yandex;
// use yii\filters\AccessControl;
use yii\web\Controller;
// use app\models\Product;
// use app\models\LoginForm;
// use yii\filters\VerbFilter;
// use yii\data\ActiveDataProvider;
// use app\models\Cart;
// use yii\web\NotFoundHttpException;
// use app\models\Order;
use yii\filters\VerbFilter;


class YandexController extends Controller
{
    public $enableCsrfValidation = false;


    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'check' => ['post'],
                ],
            ],
        ];
    }

    public function actionCheck()
    {
    	date_default_timezone_set('Europe/Moscow');
        Yii::info(json_encode($_POST), 'yandex');

        header("Content-Type: text/xml");

        echo '<?xml version="1.0" encoding="UTF-8"?>
            <checkOrderResponse
            performedDatetime="'.date(DATE_ISO8601).'"
            code="0"
            invoiceId="'.$_POST['invoiceId'].'"
            orderSumAmount="'.$_POST['orderSumAmount'].'"
            message="test"
            techMessage="test"
            shopId="'.Yandex::SHOPID.'"/>';
    }

// ShopID = 27611
// scid = 58480
    public function actionFail()
    {
        Yii::info(json_encode($_POST), 'yandex');

        echo "<h1>Произошла ошибка, платёж не совершён</h1>";
    }


}
