<?php

namespace app\controllers;

use app\models\Cart;
use Yii;
use app\models\Order;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * OrderController implements the CRUD actions for Order model.
 */
class OrderController extends Controller
{
	public function behaviors()
	{
		return [
			'verbs' => [
				'class' => VerbFilter::className(),
				'actions' => [
					'delete' => ['post'],
				],
			],
		];
	}

	/**
	 * Lists all Order models.
	 * @return mixed
	 */
	public function actionIndex()
	{
		$dataProvider = new ActiveDataProvider([
			'query' => Order::find(),
		]);

		return $this->render('index', [
			'dataProvider' => $dataProvider,
		]);
	}

	/**
	 * Displays a single Order model.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionView($id)
	{
		return $this->render('view', [
			'model' => $this->findModel($id),
		]);
	}

	/**
	 * Creates a new Order model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate()
	{
		$model = new Order();

		if ($model->load(Yii::$app->request->post()) && $model->save()) {

			foreach ( \Yii::$app->cart->getPositions() as $position ) {
				$cart_model = new Cart();
				$cart_model->product_id = $position->id;
				$cart_model->order_id = $model->id;
				$cart_model->count = $position->getQuantity();
				$cart_model->save();
			}
			\Yii::$app->cart->removeAll();

			$model->sendHello();
			$model->sendAlarm();


			if ($model->mode_payment > 0) {
				return $this->renderPartial('yandex', ['order'=>$model]);
			} else {
				return $this->redirect(['success']);
			}
		} else {
			return $this->render('create', [
				'model' => $model,
			]);
		}
	}

	public function actionSuccess()
	{
		return $this->render('success');
	}

	// public function actionYandex($order_id)
	// {

	// 	$order = Order::findOne($order_id);
	// 	if (!$order) {
	// 		throw new NotFoundHttpException('The requested page does not exist.');
	// 	} else {
	// 		return $this->render('yandex', ['order'=>$order]);
	// 	}
	// }


	/**
	 * Updates an existing Order model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionUpdate($id)
	{
		$model = $this->findModel($id);

		if ($model->load(Yii::$app->request->post()) && $model->save()) {
			return $this->redirect(['view', 'id' => $model->id]);
		} else {
			return $this->render('update', [
				'model' => $model,
			]);
		}
	}

	/**
	 * Deletes an existing Order model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionDelete($id)
	{
		$this->findModel($id)->delete();

		return $this->redirect(['index']);
	}

	/**
	 * Finds the Order model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 * @param integer $id
	 * @return Order the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel($id)
	{
		if (($model = Order::findOne($id)) !== null) {
			return $model;
		} else {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}

	// public function actionTest()
	// {
	// 	return $this->renderPartial('yandex');
	// }
}
