<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use app\models\Product;
use app\models\LoginForm;
use yii\filters\VerbFilter;
use yii\data\ActiveDataProvider;
use app\models\Cart;
use yii\web\NotFoundHttpException;
use app\models\Order;


class SiteController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }


    public function actionUpdateCart($id)
    {
        $position = \Yii::$app->cart->getPositionById($id);
        return $this->render('updateCart', [ 'position' => $position]);
    }

    public function actionLogin()
    {
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Страница с царями
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Страница со всеми правителями
     * @return string
     */
    public function actionGovernments()
    {
        return $this->render('governments');
    }

    public function actionMap()
    {
        return $this->render('map');
    }


}
