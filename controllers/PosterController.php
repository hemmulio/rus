<?php
/**
 * Created by PhpStorm.
 * User: ganbarov_emin
 * Date: 07/03/15
 * Time: 02:53
 */

namespace app\controllers;

use app\models\Product;
use Yii;
use app\models\Cart;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Html;

class PosterController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $selected = new Product();

        return $this->render('index', [
            'selected' => $selected,
            'count'    => 1,
        ]);
    }

    /**
     * Добавляем плакат в корзину
     */
    public function actionAdd()
    {

        $cart = \Yii::$app->cart;

        if (isset($_GET['id'])) {
            $id = $_GET['id'];
            $selected = $cart->getPositionById($id);
        }

        $border = isset($_POST['border']) ? $_POST['border'] : false;
        $count = isset($_POST['count']) ? (int)$_POST['count'] : false;
        $cut = isset($_POST['cut']) ? $_POST['cut'] : false;

        if ($border !== false && $cut !== false){
            $model = Product::find()
                ->where([
                    'type'  => 1,
                    'color' => $border,
                    'cut'   => $cut,
                ])
                ->one();
            if ($count !== false && $model) {
                // удаляю, если это обновление
                if (isset($selected) && $selected) {
                    $cart->remove($selected);
                }
                $cart->put($model, $count);

                echo $count;
                exit;
            }

        }
        echo '0';
    }

    /**
     * Updates an existing Cart model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $selected = Yii::$app->cart->getPositionById($id);
        $selected = $selected ? $selected : new Product();

        return $this->render('index', [
            'selected'=>$selected,
            'count' => $selected->getQuantity()
        ]);
    }
}