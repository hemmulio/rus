<?php

namespace app\controllers;

use app\models\Product;
use Yii;
use app\models\Cart;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CartController implements the CRUD actions for Cart model.
 */
class CartController extends Controller
{

	public function behaviors()
	{
		return [
			'verbs' => [
				'class' => VerbFilter::className(),
				'actions' => [
					'delete' => ['post'],
				],
			],
		];
	}


	public function actionIndex()
	{
		// $selected = Product::find()->where(['available'=>1])
		//	 ->limit(1)->all();

		$selected = new Product;
		// $selected = Product::find()->one();
		// var_dump($selected->color);
		// $selected = $selected ? $selected[0] : new Product;


		$availibles = Product::getAvailibles($selected->cut, $selected->color, $selected->size);

		// var_dump($availibles);


		return $this->render('index', [
			'selected'=>$selected,
			'availibles' => $availibles,
			'count' => 1,
		]);
	}

	public function actionCart()
	{
		return $this->render('cart');
	}

	public function actionAdd()
	{

		$cart = \Yii::$app->cart;

		if (isset($_GET['id'])) {
			$id = $_GET['id'];
			$selected = $cart->getPositionById($id);
		}

		$color = isset($_POST['color']) ? $_POST['color'] : false;
		$cut = isset($_POST['cut']) ? $_POST['cut'] : false;
		$size = isset($_POST['size']) ? $_POST['size'] : false;
		$count = isset($_POST['count']) ? (int)$_POST['count'] : false;

		if ($color !== false && $cut !== false && $size !== false){
			$model = Product::find()
				->where([
					'color' => $color,
					'cut' => $cut,
					'size' => $size,
				])
				->one();
			if ($count !== false && $model) {
				// удаляю, если это обновление
				if (isset($selected) && $selected) {
					$cart->remove($selected);
				}
				$cart->put($model, $count);

				echo $count;
				exit;
			}

		}
		echo '0';
	}


	/**
	 * Displays a single Cart model.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionView($id)
	{
		return $this->render('view', [
			'model' => $this->findModel($id),
		]);
	}

	/**
	 * Creates a new Cart model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate()
	{
		$model = new Cart();

		if ($model->load(Yii::$app->request->post()) && $model->save()) {
			return $this->redirect(['view', 'id' => $model->id]);
		} else {
			return $this->render('create', [
				'model' => $model,
			]);
		}
	}

	/**
	 * Updates an existing Cart model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionUpdate($id)
	{


		$selected = Yii::$app->cart->getPositionById($id);

		$selected = $selected ? $selected : new Product;


		$availibles = Product::getAvailibles(
			$selected->cut,
			$selected->color,
			$selected->size
		);

		return $this->render('index', [
			'selected'=>$selected,
			'availibles' => $availibles,
			'count' => $selected->getQuantity()
		]);
	}

	/**
	 * Deletes an existing Cart model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionDelete($id)
	{
		$this->findModel($id)->delete();

		return $this->redirect('cart');
	}

	public function actionDeleteFromCart($id)
	{
		$position = \Yii::$app->cart->getPositionById($id);
		\Yii::$app->cart->remove($position);
		return $this->redirect('cart');
	}

	/**
	 * Finds the Cart model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 * @param integer $id
	 * @return Cart the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel($id)
	{
		if (($model = Cart::findOne($id)) !== null) {
			return $model;
		} else {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}

	public function actionAvailibles()
	{
		$color = isset($_POST['color']) ? $_POST['color'] : false;
		$cut = isset($_POST['cut']) ? $_POST['cut'] : false;
		$size = isset($_POST['size']) ? $_POST['size'] : false;
		$count = isset($_POST['count']) ? $_POST['count'] : 0;


		$availibles = Product::getAvailibles($cut, $color, $size);

		$selected = new Product;

		// $view = new yii\web\View;
		echo $this->renderPartial('_palitra', [
			'selcut'=>$cut,
			'selcolor'=>$color,
			'selsize'=>$size,
			'count'=>$count,
			'selected'=>$selected,

			'availibles'=>$availibles
		]);

	}

}

