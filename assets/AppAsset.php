<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/site.css',
        'css/social_likes_birman.css',
        // 'css/layout.css',

    ];
    public $js = [
        'js/send_to_cart.js',
        'js/groups_availible.js',
        'js/short_color.js',
        'js/social_likes.min.js',
        'js/tzars.js',
        'js/tzars_bubble.js',
        // 'js\layout.js',

    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\web\JqueryAsset',

    ];
}
