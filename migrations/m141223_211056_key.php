<?php

use yii\db\Schema;
use yii\db\Migration;

class m141223_211056_key extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE `cart`
            DROP FOREIGN KEY `cart_ibfk_2`,
            ADD FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) ON DELETE CASCADE ON UPDATE CASCADE");

        $this->execute("INSERT INTO `product` (`description`, `price`, `cut`, `color`, `size`, `available`) VALUES
            ('футблолка',  1000.00, 'Женский',  'blue',    'XXXL',   0),
            ('футблолка',  1000.00, 'Женский',  'green',   'XXXL',   0),
            ('футблолка',  1000.00, 'Женский',  'red',     'XXXL',   0),
            ('футблолка',  1000.00, 'Женский',  'white',   'XXXL',   0),
            ('футблолка',  1000.00, 'Женский',  'yellow',  'XXXL',   0),
            ('футблолка',  1000.00, 'Женский',  'black',   'XXXL',   0),
            ('футблолка',  1000.00, 'Женский',  'pink',    'XXXL',   0),
            ('футблолка',  1000.00, 'Женский',  'orange',  'XXXL',   0),
            ('футблолка',  1000.00, 'Мужской',  'blue',    'XXXL',   0),
            ('футблолка',  1000.00, 'Мужской',  'green',   'XXXL',   0),
            ('футблолка',  1000.00, 'Мужской',  'red',     'XXXL',   0),
            ('футблолка',  1000.00, 'Мужской',  'white',   'XXXL',   0),
            ('футблолка',  1000.00, 'Мужской',  'yellow',  'XXXL',   0),
            ('футблолка',  1000.00, 'Мужской',  'black',   'XXXL',   0),
            ('футблолка',  1000.00, 'Мужской',  'pink',    'XXXL',   0),
            ('футблолка',  1000.00, 'Мужской',  'orange',  'XXXL',   0)");

        $this->execute("UPDATE product SET available = 1");
        $this->execute("UPDATE product SET available = 0 WHERE cut='женский' AND (size='XXXL'  OR size='XXL')");

    }

    public function down()
    {
        echo "m141223_211056_key cannot be reverted.\n";

        return false;
    }
}
