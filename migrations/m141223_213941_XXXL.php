<?php

use yii\db\Schema;
use yii\db\Migration;

class m141223_213941_XXXL extends Migration
{
    public function up()
    {
         $this->execute("ALTER TABLE `product`
                 CHANGE `size` `size` enum('S','M','L','XL','XXL','XXXL') COLLATE 'utf8_general_ci' NOT NULL AFTER `color`,
                 COMMENT=''");
    }

    public function down()
    {
        echo "m141223_213941_XXXL cannot be reverted.\n";

        return false;
    }
}
