<?php

use yii\db\Schema;
use yii\db\Migration;

class m150306_222229_add_poster_data_to_product extends Migration
{
    public function safeUp()
    {
        $this->insert('product', ['price' => 2490, 'cut' => 0, 'color' => 0, 'size' => 0, 'available' => 1, 'type' => 1, 'description' => 'бумага 250 г/м. размер 100х70 см.']);
        $this->insert('product', ['price' => 3690, 'cut' => 1, 'color' => 1, 'size' => 0, 'available' => 1, 'type' => 1, 'description' => 'бумага 250 г/м. размер 100х70 см.']);
        $this->insert('product', ['price' => 3690, 'cut' => 1, 'color' => 2, 'size' => 0, 'available' => 1, 'type' => 1, 'description' => 'бумага 250 г/м. размер 100х70 см.']);

        return true;
    }

    public function safeDown()
    {
        $this->delete('product', 'type = 1');

        return true;
    }
}
