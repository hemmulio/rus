<?php

use yii\db\Schema;
use yii\db\Migration;

class m141214_111728_init extends Migration
{
    public function up()
    {
        $this->execute("-- Adminer 4.1.0 MySQL dump

            SET NAMES utf8;
            SET time_zone = '+00:00';
            SET foreign_key_checks = 0;
            SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

            DROP TABLE IF EXISTS `cart`;
            CREATE TABLE `cart` (
              `id` int(11) NOT NULL AUTO_INCREMENT,
              `order_id` int(11) NOT NULL,
              `product_id` int(11) NOT NULL,
              `count` int(11) NOT NULL,
              PRIMARY KEY (`id`),
              KEY `order_id` (`order_id`),
              KEY `product_id` (`product_id`),
              CONSTRAINT `cart_ibfk_3` FOREIGN KEY (`order_id`) REFERENCES `order` (`id`) ON DELETE CASCADE,
              CONSTRAINT `cart_ibfk_2` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) ON DELETE NO ACTION
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;


            DROP TABLE IF EXISTS `order`;
            CREATE TABLE `order` (
              `id` int(11) NOT NULL AUTO_INCREMENT,
              `datatime` datetime NOT NULL,
              `status` tinyint(4) NOT NULL,
              `name` varchar(255) NOT NULL,
              `phone` varchar(255) NOT NULL,
              `email` varchar(255) NOT NULL,
              `country` varchar(255) NOT NULL,
              `region` varchar(255) NOT NULL,
              `city` varchar(255) NOT NULL,
              `address` varchar(255) NOT NULL,
              `mode_delivery` tinyint(4) NOT NULL,
              `mode_payment` tinyint(4) NOT NULL,
              `admin_comment` tinytext NOT NULL,
              `user_comment` tinytext NOT NULL,
              PRIMARY KEY (`id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;


            DROP TABLE IF EXISTS `product`;
            CREATE TABLE `product` (
              `id` int(11) NOT NULL AUTO_INCREMENT,
              `description` tinytext NOT NULL,
              `price` decimal(10,2) NOT NULL,
              `cut` tinyint(4) NOT NULL,
              `color` varchar(255) NOT NULL,
              `size` tinyint(4) NOT NULL,
              PRIMARY KEY (`id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;


            -- 2014-12-14 11:24:07");
    }

    public function down()
    {
        echo "m141214_111728_init cannot be reverted.\n";

        return true;
    }
}
