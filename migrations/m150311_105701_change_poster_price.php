<?php

use yii\db\Schema;
use yii\db\Migration;

class m150311_105701_change_poster_price extends Migration
{
    public function safeUp()
    {
        $this->update('product', ['price' => 2900], ['type' => 1, 'cut' => 0, 'color' => 0]);
        $this->update('product', ['price' => 5500], ['type' => 1, 'cut' => 1, 'color' => 1]);
        $this->update('product', ['price' => 5200], ['type' => 1, 'cut' => 1, 'color' => 2]);

        return true;
    }

    public function safeDown()
    {
        return true;
    }
}
