<?php

use yii\db\Schema;
use yii\db\Migration;

class m141217_195315_availible_product extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE `product`
            ADD `available` tinyint(4) NOT NULL DEFAULT '1',
            COMMENT='';");

        $this->execute("ALTER TABLE `product`
            CHANGE `size` `size` enum('XS','S','M','L','XL','XXL') NOT NULL AFTER `color`,
            COMMENT='';");

        $this->execute("ALTER TABLE `product`
            CHANGE `cut` `cut` enum('Мужской','Женский') NOT NULL AFTER `price`,
            COMMENT='';");

        $this->execute("ALTER TABLE `product`
            CHANGE `color` `color` enum('blue','green','red','white','yellow') COLLATE 'utf8_general_ci' NOT NULL AFTER `cut`,
            COMMENT='';");
    }

    public function down()
    {
        echo "m141217_195315_availible_product cannot be reverted.\n";

        return false;
    }
}
