<?php

use yii\db\Schema;
use yii\db\Migration;

class m150306_215526_change_t_shirt_price extends Migration
{
    public function safeUp()
    {
        $this->update('product', ['price' => 990], 'type = 0');

        return true;
    }

    public function safeDown()
    {
        $this->update('product', ['price' => 1000], 'type = 0');

        return true;
    }
}
