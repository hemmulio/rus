<?php

use yii\db\Schema;
use yii\db\Migration;

class m141223_214903_cut extends Migration
{
    public function up()
    {
         $this->execute("ALTER TABLE `product`
CHANGE `cut` `cut` enum('мужской','женский') COLLATE 'utf8_general_ci' NOT NULL AFTER `price`,
COMMENT='';");

    }

    public function down()
    {
        echo "m141223_214903_cut cannot be reverted.\n";

        return false;
    }
}
