<?php

use yii\db\Schema;
use yii\db\Migration;

class m150306_213236_add_column_type_to_product extends Migration
{
    public function safeUp()
    {
        $this->addColumn('product', 'type', 'tinyint(1) not null default 0');

        return true;
    }

    public function safeDown()
    {
        $this->dropColumn('product', 'type');

        return true;
    }
}
