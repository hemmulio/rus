<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use app\models\Product;

//use yii\captcha\Captcha;
//use kartik\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\ContactForm */

$this->title = 'Купить майку';
            // var_dump($availibles);

?>

<div class="row">
    <div class="col-md-9">
        <div class="t-short-image">
            <img id='t-short-image-big' class='image_big' src="/images/t-shirts/M_black.jpg"  />
        </div>
        <div class="subrow">
            <img id='t-short-image' class="subrow_img" src="/images/t-shirts/M_black.jpg"  />
            <img id='couple' class="subrow_img" src="/images/couple/M_black.jpg"/>
            <img id='close' class="subrow_img" src="/images/small/black.jpg"/>

        </div>
    </div>
    <div class="col-md-3">
        <div id='palitra' class="order-row">
                <?php echo $this->render('_palitra', [
                    'selcut'=>$selected->cut,
                    'selcolor'=>$selected->color,
                    'selsize'=>$selected->size,
                    'count'=>$count,
                    'selected'=>$selected,
                    'availibles'=>$availibles]);?>

        </div>

        <!-- для ошибки -->
        <div id="error" class="order-row"></div>

    </div>

</div>


