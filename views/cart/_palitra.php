<?php
use app\models\Product;
?>
<div class="description_shirt">
    <p>
        <b>Футболка</b><br>
        History of Russia <br>
        100% хлопок <br>
        плотность 155 г/м
    </p>
    <div class="price_shirt">990.-</div>
</div>

<div class="cut">
        <?php foreach (Product::$cutsAvailable as $key => $cut):
            $disable_this = $availibles['cut'][$key] ? '' : 'disabled';
            $select_this = (string)$selcut === (string)$key && !$disable_this ? 'checked' : '';
        ?>
        <div>
            <div style="line-height: 30px;">
            <label class="wrap-checkbox <?= $select_this ?> <?=$disable_this?> cut-<?= $cut?>">
                <input id="cut-<?= $key?>"
                    name="cut"
                    class="checkbox"
                    <?= $disable_this?>
                    type="checkbox"
                    <?= $select_this ?>
                    value="<?=$key?>">
            </label>

            <span class="cut-label <?=$disable_this?>">
                <?= $cut ?>
            </span>
            </div>
        </div>
        <?php endforeach ?>
</div>

<div class="color">
    <p><b>цвет</b></p>
        <?php foreach (Product::$colorsAvailable as $key=>$color) {
            $disable_this = $availibles['color'][$key] ? '' : 'disabled';
            // var_dump($disable_this);
            $select_this = (string)$selcolor === (string)$key && !$disable_this ? 'checked' : '';
            if (in_array($key, [0,4])) {
                $close_on = $key+3;
                echo "<div class=''>";
            }
        ?>
        <label class="wrap-checkbox <?= $select_this ?> <?=$disable_this?> color-<?= $color?>">
            <input
                class="checkbox"
                id="color-<?= $color?>"
                name="color"
                <?= $disable_this ?>
                type="checkbox"
                <?= $select_this ?>
                value="<?=$key?>">
        </label>
        <?php
            if (in_array($key, [3,7])) {
                echo "</div>";
            }
        } ?>

    <br>

</div>


<div class="size">
    <p><b>размер</b></p>
    <?php
     foreach (Product::$sizesAvailable as $key=>$size) {
        $disable_this = $availibles['size'][$key] ? '' : 'disabled';

        if (in_array($key, [0,2,4,6,8])) {
            echo "<div class='size_row'>";
        }
        $select_this = (string)$selsize === (string)$key && !$disable_this  ? 'checked' : '';
        ?>

        <div class="size_cell" style="line-height: 30px;">
            <label class="wrap-checkbox <?= $select_this ?> <?=$disable_this?> size-<?= $size?>">
                <input id="size_<?=$key?>" class="checkbox" <?=$disable_this?> name="size" <?= $select_this ?> type="checkbox" value=<?=$key?>>
            </label>
            <span class="cut-label <?=$disable_this?>">
                <?=$size?>
            </span>
        </div>
        <?php
        if (in_array($key, [1,3,5,7,9])) {
            echo "</div>";
        }
    } ?>

</div>

<div class="number">
    <p><b>количество</b></p>
    <div class="count_row">
        <div class="btn count-btn btn-xs minus" type="button" value="&nbsp;-&nbsp;"><span class="icon_count_minus">-</span></div>
        <input id="count" class="number_count_products"  type="text" value="<?=$count?>" size="2">
        <div class="btn count-btn btn-xs plus" type="button" value="&nbsp;+&nbsp;">+</div>
    </div>
</div>

<br>
<?php $selected_id = $selected->id ? $selected->id : 0;?>
<input id="send_to_cart" data-selected_id="<?= $selected_id ?>" class="btn btn-block" type="button" value="добавить в корзину">
