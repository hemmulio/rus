<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\Product;
use app\models\Cart;

/* @var $this yii\web\View */
/* @var $model app\models\Cart */

?>

<div class=col-md-12 id="main_content_basket">
    <h2>Корзина</h2><br/><br/>
    <?php $cart = \Yii::$app->cart; if  (\Yii::$app->cart->getCount() > 0): ?>
        <div class="row">
            <div class="col-md-5 col-grey">
                <h4>Описание товара<h4>
            </div>
            <div class="col-md-4 col-grey">
                <h4>Детали<h4>
            </div>
            <div class="col-md-3 col-grey">
                <h4>Цена<h4>
            </div>
        </div>
        <?php foreach ( $cart->getPositions() as $position ) {?>
            <div class="row" style="padding: 5px; height: 145px;">
                <div class="right-border col-md-5">
                <div>
                    <div>
                        <?php if ($position->type == 0) { ?>
                            <img class = 'img-left' src="/images/small/<?php echo Product::$colorsAvailable[$position->color] ?>.jpg"/>
                        <?php } else { ?>
                            <img class = 'img-left' src="/images/posters/<?= Product::$posterColor[$position->color] ?>_border_02.jpg"/>
                        <?php } ?>
                        <?php echo Cart::getDescription($position->description); ?><br>
                    </div>
                </div>
                </div>
                <div class="right-border col-md-4">
                <p>
                    <?php if ($position->type == 0) { ?>
                        Цвет: <?php echo $position->getColorRu($position->color);?><br>
                        Крой: <?php echo Product::$cutsAvailable[$position->cut];?><br>
                        Размер: <?php echo Product::$sizesAvailable[$position->size];?><br>
                        Количество: <?php echo $position->getQuantity();?><br>
                        <?php echo Html::a('Изменить детали', ['cart/update', 'id' => $position->id], ['class' => 'btn-my btn-info-my'] ) ?>
                    <?php } else { ?>
                        Тип: <?= ($position->cut == 1) ? 'с рамкой' : 'без рамки'; ?><br/>
                        <?php
                            if ($position->cut == 1) {
                                echo 'Цвет: ' .  Product::$posterColorRus[$position->color] . '<br/>';
                            }
                        ?>
                        Количество: <?php echo $position->getQuantity();?><br>
                        <?php echo Html::a('Изменить детали', ['poster/update', 'id' => $position->id], ['class' => 'btn-my btn-info-my'] ) ?>
                    <?php } ?>
                </p>
                </div>
                <div class="col-md-3">
                    <?php echo $position->price; ?> руб
                </div>
            </div>
            <div class = "row ">
                <div class = "col-md-12">
                    <?php echo Html::a('Удалить', ['cart/delete-from-cart', 'id' => $position->id], ['class' => 'font-black']) ?>
                </div>
            </div>
            <hr >
        <?php }?>
        <div class="row">
            <div class="col-md-5">
            </div>
            <div class="col-md-4">
                <div> Итого </div>
            </div>
            <div class="col-md-3">
                <?php echo $cart->getCost().' руб' ?>
            </div>
        </div>
        <?php echo Html::a('КУПИТЬ', ['order/create', 'id' => $position->id], ['class' => 'btn-my btn-info-my-2 pull-right btn-level']) ?>
    <?php else: ?>
        Корзина пуста.
    <?php endif; ?>
</div>

