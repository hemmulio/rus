<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
//use yii\captcha\Captcha;
//use kartik\widgets\ActiveForm;
use yii\db\Query;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\ContactForm */

$this->title = 'Contact';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-contact">

    <div class="t-short-image">
        <img id='t-short-image' src=<?php echo "/images/".$position->color.".jpg"?> width="100%"  /></a>
    </div>

    <div class="order-row">

            <div class="cut">
                <p><b>Тип кроя</b></p>
                <p><input name="cut" type="radio" value="0" <?php if ($position->cut === 0) echo 'checked'?>>Мужской</p>
                <p><input name="cut" type="radio" value="1" <?php if ($position->cut === 1) echo 'checked'?>>Женский</p>
            </div>

            <div class="color">
                <p><b>Цвет</b></p>
                <div class="">
                    <input name="color" type="radio" value="green" <?php if ($position->color === 'green') echo 'checked'?>>
                    <input name="color" type="radio" value="yellow" <?php if ($position->color === 'yellow') echo 'checked'?>>
                    <input name="color" type="radio" value="orange" <?php if ($position->color === 'orange') echo 'checked'?>>
                    <input name="color" type="radio" value="black-orange" <?php if ($position->color === 'black-orange') echo 'checked'?>>
                    <input name="color" type="radio" value="red" <?php if ($position->color === 'red') echo 'checked'?>>
                </div>
                <div class="">
                    <input name="color" type="radio" value="white" <?php if ($position->color === 'white') echo 'checked'?>>
                    <input name="color" type="radio" value="grey" <?php if ($position->color === 'grey') echo 'checked'?>>
                    <input name="color" type="radio" value="blue" <?php if ($position->color === 'blue') echo 'checked'?>>
                    <input name="color" type="radio" value="azure" <?php if ($position->color === 'azure') echo 'checked'?>>
                    <input name="color" type="radio" value="black" <?php if ($position->color === 'black') echo 'checked'?>>
                </div>
            </div>

            <div class="size">
                <p><b>Размер</b></p>
                <div class="">
                    <input name="size" type="radio" value="1" <?php if ($position->cut === 1) echo 'checked'?>>S
                    <input name="size" type="radio" value="2" <?php if ($position->cut === 2) echo 'checked'?>>M
                </div>
                <div class="">
                    <input name="size" type="radio" value="3" <?php if ($position->cut === 3) echo 'checked'?>>L
                    <input name="size" type="radio" value="4" <?php if ($position->cut === 4) echo 'checked'?>>XL
                </div>
                <div class="">
                    <input name="size" type="radio" value="5" <?php if ($position->cut === 5) echo 'checked'?>>XXL
                    <input name="size" type="radio" value="6" <?php if ($position->cut === 6) echo 'checked'?>>XXXL
                </div>
            </div>

            <div class="number">
                <p><b>Количество</b></p>
                <span class="minus">-</span>
                <input id="count" type="text" <?php echo 'value='.$position->getQuantity() ?> size="2"/>
                <span class="plus">+</span>
            </div>

            <input id="send_to_cart" type="submit">

    </div>

</div>
