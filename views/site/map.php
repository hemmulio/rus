<?php
// use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use kartik\widgets\Select2;
use yii\web\JsExpression;
use app\models\Product;

//use yii\captcha\Captcha;
//use kartik\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\ContactForm */

$this->title = 'Контакты';

/*$form = ActiveForm::begin([
    'id' => 'login-form',
    'options' => ['class' => 'form-horizontal map'],
    'fieldConfig' => [
        'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-8\">{error}</div>",
        'labelOptions' => ['class' => 'col-lg-1 control-label'],
    ],
]);

// The controller action that will render the list
$url = \yii\helpers\Url::to(['city-list']);

// Script to initialize the selection based on the value of the select2 element
$initScript = <<< SCRIPT
function (element, callback) {
    var id=\$(element).val();
    if (id !== "") {
        \$.ajax("{$url}?id=" + id, {
            dataType: "json"
        }).done(function(data) { callback(data.results);});
    }
}
SCRIPT;
$model = new Product;

// The widget
echo $form->field($model, 'id')->widget(Select2::classname(), [
    'options' => ['placeholder' => 'Search for a city ...'],
    'pluginOptions' => [
        'allowClear' => true,
        'minimumInputLength' => 3,
        'ajax' => [
            'url' => $url,
            'dataType' => 'json',
            'data' => new JsExpression('function(term,page) { return {search:term}; }'),
            'results' => new JsExpression('function(data,page) { return {results:data.results}; }'),
        ],
        'initSelection' => new JsExpression($initScript)
    ],
]);*/
?>
<?php //ActiveForm::end(); ?>
<div class=col-md-12 id="contact_block">
<br>
<p>
Доставка курьером осуществляется по Москве в пределах МКАД. <br>
Стоимость доставки футболки — 300 Р.<br>
Стоимость доставки плаката — 500 Р. <br>
Наш менеджер свяжется с Вами
для подтверждения заказа.<br>
</p>




<p>Возможна доставка почтой России.<br></p>
<p>
    +7 (499) 157-7734<br>
    +7 (968) 961-3062<br>
    info@tzars.ru<br>
    <a href="https://www.facebook.com/pages/%D0%98%D1%81%D1%82%D0%BE%D1%80%D0%B8%D1%8F-%D0%93%D0%BE%D1%81%D1%83%D0%B4%D0%B0%D1%80%D1%81%D1%82%D0%B2%D0%B0-%D0%A0%D0%BE%D1%81%D1%81%D0%B8%D1%81%D0%BA%D0%BE%D0%B3%D0%BE/202857043222614">Facebook</a><br>
    <a href="http://vk.com/public84919649">Вконтакте</a><br>
    Самостоятельно забрать заказ можно в нашем офисе: <br>
    Москва, Викторенко 2/1, 3-й подъезд, 8-й этаж, домофон 95.<br>
</p>
<script type="text/javascript" charset="utf-8" src="//api-maps.yandex.ru/services/constructor/1.0/js/?sid=emXHAo21DvX_DWt8DU6WA8LoK4ndlJiZ&width=940&height=450"></script>
</div>
