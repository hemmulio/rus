<?php
/* @var $this yii\web\View */

$this->title = 'Правители';
?>
<div class="social-likes">
    <div class="facebook" title="Поделиться ссылкой на Фейсбуке"></div>
    <div class="twitter" title="Поделиться ссылкой в Твиттере"></div>
    <div class="vkontakte" title="Поделиться ссылкой во Вконтакте"></div>
</div>
<div id="caption_all_government_page">ПРАВИТЕЛИ РОССИИ</div>
<div id="main_kings_block">
    <div id="list_government">
        <div id="row_01">
            <div id="government_01" class="government">
                <div id="bubble_01" data-show="0" class="bubble"></div>
            </div>
            <div id="government_02" class="government">
                <div id="bubble_02" data-show="0" class="bubble"></div>
            </div>
            <div id="government_03" class="government">
                <div id="bubble_03" data-show="0" class="bubble"></div>
            </div>
            <div id="government_04" class="government">
                <div id="bubble_04" data-show="0" class="bubble"></div>
            </div>
            <div id="government_05" class="government">
                <div id="bubble_05" data-show="0" class="bubble"></div>
            </div>
            <div id="government_06" class="government">
                <div id="bubble_06" data-show="0" class="bubble"></div>
            </div>
            <div id="government_07" class="government">
                <div id="bubble_07" data-show="0" class="bubble"></div>
            </div>
            <div id="government_08" class="government">
                <div id="bubble_08" data-show="0" class="bubble"></div>
            </div>
            <div id="government_09" class="government">
                <div id="bubble_09" data-show="0" class="bubble"></div>
            </div>
            <div id="government_10" class="government">
                <div id="bubble_10" data-show="0" class="bubble"></div>
            </div>
            <div id="government_11" class="government">
                <div id="bubble_11" data-show="0" class="bubble"></div>
            </div>
        </div>
        <div id="row_02">
            <div id="government_12" class="government">
                <div id="bubble_12" data-show="0" class="bubble"></div>
            </div>
            <div id="government_13" class="government">
                <div id="bubble_13" data-show="0" class="bubble"></div>
            </div>
            <div id="government_14" class="government">
                <div id="bubble_14" data-show="0" class="bubble"></div>
            </div>
            <div id="government_15" class="government">
                <div id="bubble_15" data-show="0" class="bubble"></div>
            </div>
            <div id="government_16" class="government">
                <div id="bubble_16" data-show="0" class="bubble"></div>
            </div>
            <div id="government_17" class="government">
                <div id="bubble_17" data-show="0" class="bubble"></div>
            </div>
            <div id="government_18" class="government">
                <div id="bubble_18" data-show="0" class="bubble"></div>
            </div>
            <div id="government_19" class="government">
                <div id="bubble_19" data-show="0"class="bubble"></div>
            </div>
            <div id="government_20" class="government">
                <div id="bubble_20" data-show="0" class="bubble"></div>
            </div>
            <div id="government_21" class="government">
                <div id="bubble_21" data-show="0" class="bubble"></div>
            </div>
            <div id="government_22" class="government">
                <div id="bubble_22" data-show="0" class="bubble"></div>
            </div>
        </div>
        <div id="row_03">
            <div id="government_23" class="government">
                <div id="bubble_23" data-show="0" class="bubble"></div>
            </div>
            <div id="government_24" class="government">
                <div id="bubble_24" data-show="0" class="bubble"></div>
            </div>
            <div id="government_25" class="government">
                <div id="bubble_25" data-show="0" class="bubble"></div>
            </div>
            <div id="government_26" class="government">
                <div id="bubble_26" data-show="0" class="bubble"></div>
            </div>
            <div id="government_27" class="government">
                <div id="bubble_27" data-show="0" class="bubble"></div>
            </div>
            <div id="government_28" class="government">
                <div id="bubble_28" data-show="0" class="bubble"></div>
            </div>
            <div id="government_29" class="government">
                <div id="bubble_29" data-show="0" class="bubble"></div>
            </div>
            <div id="government_30" class="government">
                <div id="bubble_30" data-show="0" class="bubble"></div>
            </div>
            <div id="government_31" class="government">
                <div id="bubble_31" data-show="0" class="bubble"></div>
            </div>
            <div id="government_32" class="government">
                <div id="bubble_32" data-show="0" class="bubble"></div>
            </div>
            <div id="government_33" class="government">
                <div id="bubble_33" data-show="0" class="bubble"></div>
            </div>
        </div>
        <div id="row_04">
            <div id="government_34" class="government">
                <div id="bubble_34" data-show="0" class="bubble"></div>
            </div>
            <div id="government_35" class="government">
                <div id="bubble_35" data-show="0" class="bubble"></div>
            </div>
            <div id="government_36" class="government">
                <div id="bubble_36" data-show="0" class="bubble"></div>
            </div>
            <div id="government_37" class="government">
                <div id="bubble_37" data-show="0" class="bubble"></div>
            </div>
            <div id="government_38" class="government">
                <div id="bubble_38" data-show="0" class="bubble"></div>
            </div>
            <div id="government_39" class="government">
                <div id="bubble_39" data-show="0" class="bubble"></div>
            </div>
            <div id="government_40" class="government">
                <div id="bubble_40" data-show="0" class="bubble"></div>
            </div>
            <div id="government_41" class="government">
                <div id="bubble_41" data-show="0" class="bubble"></div>
            </div>
            <div id="government_42" class="government">
                <div id="bubble_42" data-show="0" class="bubble"></div>
            </div>
            <div id="government_43" class="government">
                <div id="bubble_43" data-show="0" class="bubble"></div>
            </div>
            <div id="government_44" class="government">
                <div id="bubble_44" data-show="0" class="bubble"></div>
            </div>
        </div>
        <div id="row_05">
            <div id="government_45" class="government">
                <div id="bubble_45" data-show="0" class="bubble"></div>
            </div>
            <div id="government_46" class="government">
                <div id="bubble_46" data-show="0" class="bubble"></div>
            </div>
            <div id="government_47" class="government">
                <div id="bubble_47" data-show="0" class="bubble"></div>
            </div>
            <div id="government_48" class="government">
                <div id="bubble_48" data-show="0" class="bubble"></div>
            </div>
            <div id="government_49" class="government">
                <div id="bubble_49" data-show="0" class="bubble"></div>
            </div>
            <div id="government_50" class="government">
                <div id="bubble_50" data-show="0" class="bubble"></div>
            </div>
            <div id="government_51" class="government">
                <div id="bubble_51" data-show="0" class="bubble"></div>
            </div>
            <div id="government_52" class="government">
                <div id="bubble_52" data-show="0" class="bubble"></div>
            </div>
            <div id="government_53" class="government">
                <div id="bubble_53" data-show="0" class="bubble"></div>
            </div>
            <div id="government_54" class="government">
                <div id="bubble_54" data-show="0" class="bubble"></div>
            </div>
            <div id="government_55" class="government">
                <div id="bubble_55" data-show="0" class="bubble"></div>
            </div>
        </div>
        <div id="row_06">
            <div id="government_56" class="government">
                <div id="bubble_56" data-show="0" class="bubble"></div>
            </div>
            <div id="government_57" class="government">
                <div id="bubble_57" data-show="0" class="bubble"></div>
            </div>
            <div id="government_58" class="government">
                <div id="bubble_58" data-show="0" class="bubble"></div>
            </div>
            <div id="government_59" class="government">
                <div id="bubble_59" data-show="0" class="bubble"></div>
            </div>
            <div id="government_60" class="government">
                <div id="bubble_60" data-show="0" class="bubble"></div>
            </div>
            <div id="government_61" class="government">
                <div id="bubble_61" data-show="0" class="bubble"></div>
            </div>
            <div id="government_62" class="government">
                <div id="bubble_62" data-show="0" class="bubble"></div>
            </div>
            <div id="government_63" class="government">
                <div id="bubble_63" data-show="0" class="bubble"></div>
            </div>
            <div id="government_64" class="government">
                <div id="bubble_64" data-show="0" class="bubble"></div>
            </div>
            <div id="government_65" class="government">
                <div id="bubble_65" data-show="0" class="bubble"></div>
            </div>
            <div id="government_66" class="government">
                <div id="bubble_66" data-show="0" class="bubble"></div>
            </div>
        </div>
        <div id="row_07">
            <div id="government_67" class="government">
                <div id="bubble_67" data-show="0" class="bubble"></div>
            </div>
            <div id="government_68" class="government">
                <div id="bubble_68" data-show="0" class="bubble"></div>
            </div>
            <div id="government_69" class="government">
                <div id="bubble_69" data-show="0" class="bubble"></div>
            </div>
            <div id="government_70" class="government">
                <div id="bubble_70" data-show="0" class="bubble"></div>
            </div>
            <div id="government_71" class="government">
                <div id="bubble_71" data-show="0" class="bubble"></div>
            </div>
            <div id="government_72" class="government">
                <div id="bubble_72" data-show="0" class="bubble"></div>
            </div>
            <div id="government_73" class="government">
                <div id="bubble_73" data-show="0" class="bubble"></div>
            </div>
            <div id="government_74" class="government">
                <div id="bubble_74" data-show="0" class="bubble"></div>
            </div>
            <div id="government_75" class="government">
                <div id="bubble_75" data-show="0" class="bubble"></div>
            </div>
            <div id="government_76" class="government">
                <div id="bubble_76" data-show="0" class="bubble"></div>
            </div>
            <div id="government_77" class="government">
                <div id="bubble_77" data-show="0" class="bubble"></div>
            </div>
        </div>
        <div id="row_08">
            <div id="government_78" class="government">
                <div id="bubble_78" data-show="0" class="bubble"></div>
            </div>
            <div id="government_79" class="government">
                <div id="bubble_79" data-show="0" class="bubble"></div>
            </div>
            <div id="government_80" class="government">
                <div id="bubble_80" data-show="0" class="bubble"></div>
            </div>
            <div id="government_81" class="government">
                <div id="bubble_81" data-show="0" class="bubble"></div>
            </div>
            <div id="government_82" class="government">
                <div id="bubble_82" data-show="0" class="bubble"></div>
            </div>
            <div id="government_83" class="government">
                <div id="bubble_83" data-show="0" class="bubble"></div>
            </div>
            <div id="government_84" class="government">
                <div id="bubble_84" data-show="0" class="bubble"></div>
            </div>
            <div id="government_85" class="government">
                <div id="bubble_85" data-show="0" class="bubble"></div>
            </div>
            <div id="government_86" class="government">
                <div id="bubble_86" data-show="0" class="bubble"></div>
            </div>
            <div id="government_87" class="government">
                <div id="bubble_87" data-show="0" class="bubble"></div>
            </div>
            <div id="government_88" class="government">
                <div id="bubble_88" data-show="0" class="bubble"></div>
            </div>
            <div id="government_89" class="government">
                <div id="bubble_89" data-show="0" class="bubble"></div>
            </div>
        </div>
    </div>
</div>
