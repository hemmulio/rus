<?php
/* @var $this yii\web\View */

$this->title = 'Цари';
?>
<div class="social-likes">
    <div class="facebook" title="Поделиться ссылкой на Фейсбуке"></div>
    <div class="twitter" title="Поделиться ссылкой в Твиттере"></div>
    <div class="vkontakte" title="Поделиться ссылкой во Вконтакте"></div>
</div>
<div id="main_kings_block">
    <div id="dialog_governments"></div>
</div>