<?php
/**
 * Created by PhpStorm.
 * User: ganbarov_emin
 * Date: 07/03/15
 * Time: 02:57
 */
use yii\helpers\Html;
use app\models\Product;

/* @var $this yii\web\View */

$this->title = 'Купить плакат';
?>

<div class="row">
    <div class="col-md-9">
        <div class="t-short-image">
            <!-- Посмотреть, как сделано в футблоках и сделать так же смену картинок -->
            <img id='t-short-image-big' class='image_big' src="/images/posters/transparent_border_01.jpg"  />
        </div>
        <div class="subrow">
            <img id='t-short-image' class="subrow_img poster" src="/images/posters/transparent_border_01.jpg"  />
            <img id='couple' class="subrow_img poster" src="/images/posters/transparent_border_02.jpg"/>
            <img id='close' class="subrow_img poster" src="/images/posters/transparent_border_03.jpg"/>
            <img id='end' class="subrow_img poster" src="/images/posters/transparent_border_04.jpg"/>
        </div>
    </div>
    <div class="col-md-3">
        <div id='palitra' class="order-row">
            <!-- Передать в палитру все необходимые данные -->
            <?php echo $this->render('_palitra', [
                'selected' => $selected,
                'count'    => $count,
            ]);?>
        </div>

        <!-- для ошибки -->
        <div id="error" class="order-row"></div>

    </div>

</div>