<?php
/**
 * Created by PhpStorm.
 * User: ganbarov_emin
 * Date: 07/03/15
 * Time: 04:28
 */
use app\models\Product;
?>

<?php
    /**
     * @var int     $count
     * @var Product $selected
     */
?>

<div class="description_shirt">
    <p>
        <b>Плакат</b><br>
        История Государства Российского <br>
        шелкотрафаретная печать <br>
        бумага 250 г/м <br>
        размер 100х70 см
    </p>
    <div class="price_shirt">
        <span id="cost_poster">2900.-</span><br>
    </div>
</div>

<div class="cut poster" id="poster_param">
        <div>рамка</div>
        <div>
            <div style="line-height: 30px;">
                <label class="wrap-checkbox <?= ($selected->cut == 0 && $selected->color == 0) ? 'checked' : '' ?>">
                    <input name="border" class="checkbox" type="checkbox" value="0" <?= ($selected->cut == 0 && $selected->color == 0) ? 'checked' : '' ?>>
                </label>
                <span class="cut-label poster">без рамки</span>
            </div>
        </div>
        <div>
            <div style="line-height: 30px;">
                <label class="wrap-checkbox <?= ($selected->cut == 1 && $selected->color == 1) ? 'checked' : '' ?>">
                    <input name="border" class="checkbox" type="checkbox" value="1" <?= ($selected->cut == 1 && $selected->color == 1) ? 'checked' : '' ?>>
                </label>
                <span class="cut-label poster">коричневая</span>
            </div>
        </div>
        <div>
            <div style="line-height: 30px;">
                <label class="wrap-checkbox <?= ($selected->cut == 1 && $selected->color == 2) ? 'checked' : '' ?>">
                    <input name="border" class="checkbox" type="checkbox" value="2" <?= ($selected->cut == 1 && $selected->color == 2) ? 'checked' : '' ?>>
                </label>
                <span class="cut-label poster">серебристая</span>
            </div>
        </div>
</div>

<div class="number">
    <p><b>количество</b></p>
    <div class="count_row">
        <div class="btn count-btn btn-xs minus" type="button" value="&nbsp;-&nbsp;"><span class="icon_count_minus">-</span></div>
        <input id="count" class="number_count_products"  type="text" value="1" size="2">
        <div class="btn count-btn btn-xs plus" type="button" value="&nbsp;+&nbsp;">+</div>
    </div>
</div>
<br>
<input id="send_to_cart" data-selected_id="1" class="btn btn-block" type="button" value="добавить в корзину">