<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Product;

/* @var $this yii\web\View */
/* @var $model app\models\Product */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="product-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'price')->textInput(['maxlength' => 10]) ?>

    <?= $form->field($model, 'cut')->dropDownList(Product::$cutsAvailable, ['prompt' => '']) ?>

    <?= $form->field($model, 'color')->dropDownList(Product::$colorsRu, ['prompt' => '']) ?>

    <?= $form->field($model, 'size')->dropDownList(Product::$sizesAvailable, ['prompt' => '']) ?>

    <?= $form->field($model, 'available')->dropDownList([ 'Не доступен', 'Доступен' ]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
