<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Майки';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Добавить', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'description:ntext',
            'price',
            [
                'attribute' => 'cut',
                'value' => function ($data) {
                    return $data->cutText; // $data['name'] for array data, e.g. using SqlDataProvider.
                },
            ],
            [
                'attribute' => 'color',
                'value' => function ($data) {
                    return $data->colorRu; // $data['name'] for array data, e.g. using SqlDataProvider.
                },
            ],
            [
                'attribute' => 'size',
                'value' => function ($data) {
                    return $data->sizeText; // $data['name'] for array data, e.g. using SqlDataProvider.
                },
            ],
            [
                'attribute' => 'available',
                'value' => function ($data) {
                    return ['Нет', 'Да'][$data->available];
                },
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'template'=>'{update}  {delete}'
            ],
        ],
    ]); ?>

</div>
