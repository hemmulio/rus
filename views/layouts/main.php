<?php
use yii\helpers\Html;
use yii\helpers\Url;
use app\assets\AppAsset;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<div id="overlay"></div>
<head>
    <meta charset="<?= Yii::$app->charset ?>"/>
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
    <div class="wrap container" id="<?= (Yii::$app->request->url == '/tzars/all') ? 'page_tzars_all' : ''; ?>">
        <?php
            $count = \Yii::$app->cart->getCount() ? \Yii::$app->cart->getCount() : '';
            $cart_class = 'empty';
            if ($count) {
                $cart_class = 'not_empty';
            }
        ?>
        <div class="row navigation">
            <div class="col-md-10">
                <a href=<?= Url::to(['site/map']); ?>  class="pull-right item_nav">
                    контакты
                </a>
                <a class="navbar-brand" id="main_logo" href="/">
                    <img src="/images/logo.png" width="180" id="<?= (Yii::$app->request->url == '/tzars/all') ? 'logo_page_tzars_all' : ''; ?>">
                </a>
                <a href=<?= Url::to(['t-shirt/index']); ?>  class="pull-right item_nav">
                    футболки
                </a>
                <a href=<?= Url::to(['poster/index']); ?>  class="pull-right item_nav" id="menu_caption_poster">
                    плакаты
                </a>
                <a href=<?= Url::to(['site/governments']); ?>  class="pull-right item_nav" id="menu_caption_tzars">
                    цари
                </a>
            </div>
            <div class="col-md-2">
                <span>
                    <a href="/cart/cart" class="pull-left item_nav cart_nav">
                        <span id="cart-img" class="<?= ($count > 0) ? 'full' : ''; ?>"></span>
                        <span id="radius-wrap" class="hide_on_empty <?=$cart_class?>">
                            <span id="cart-radius">&nbsp;</span>
                            <span id="cart_count" data-count="<?=(int)$count?>"><?=$count?></span>
                        </span>
                        &nbsp;
                    </a>
                </span>
            </div>
        </div>
        <!--  Содержимое view файла, который рендерится в контроллере -->
        <?= $content ?>
    </div>
<?php $this->endBody() ?>

<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter28364416 = new Ya.Metrika({id:28364416,
                    webvisor:true,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true});
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="//mc.yandex.ru/watch/28364416" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->

</body>
</html>
<?php $this->endPage() ?>

