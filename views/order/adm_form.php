<?php

use yii\helpers\Html;
// use yii\widgets\ActiveForm;
use \kartik\widgets\ActiveForm;
use app\models\Order;
// use kartik\form\ActiveField;
/* @var $this yii\web\View */
/* @var $model app\models\Order */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="order-form">

    <?php $form = ActiveForm::begin([
        'type' => ActiveForm::TYPE_HORIZONTAL,
        'formConfig' => ['labelSpan' => 3, 'deviceSize' => ActiveForm::SIZE_SMALL],
        // options => [class => 'col-sm-5'];
    ]); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => 255]) ?>
    <?= $form->field($model, 'email')->textInput(['maxlength' => 255]) ?>
    <?= $form->field($model, 'phone')->textInput(['maxlength' => 255]) ?>

    <hr>
    <?= $form->field($model, 'mode_delivery')->dropdownlist(
        [
            '0' => 'Доставка курьером',
            '1' => 'Самовывоз',
        ]
    ) ?>
    <?= $form->field($model, 'address')->textArea(['maxlength' => 255]) ?>

    <hr>

    <?= $form->field($model, 'mode_payment')->dropdownlist(
        [
            '0' => 'Оплата наличными',
            '1' => 'Яндекс.Деньги',
        ]
    ) ?>

    <?= $form->field($model, 'status')->dropdownlist(Order::$status) ?>
    <?= $form->field($model, 'admin_comment')->textarea(['rows' => 6]) ?>



<!--     <?= $form->field($model, 'country')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'region')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'city')->textInput(['maxlength' => 255]) ?>





    <?= $form->field($model, 'user_comment')->textarea(['rows' => 6]) ?>
 -->
    <div class="form-group">
        <?= Html::submitButton('Подтвердить', ['class' => 'btn btn-success pull-right']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
