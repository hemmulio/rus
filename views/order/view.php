<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Order */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Orders', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="order-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('К списку', ['index'], ['class' => 'btn btn-default']) ?>
        <?= Html::a('Изменить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-link pull-right',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'datatime',
            [
                'attribute' => 'status',
                'value' => $model->getStatusText(),
            ],
            'name',
            'phone',
            'email:email',
            'address',
            [
                'attribute' => 'Заказ',
                'value' =>  $model->getCargo(),
            ],
            [
                'attribute' => 'Способ доставки',
                'value' => $model->getMode_deliveryText(),
            ],
            [
                'attribute' => 'Способ оплаты',
                'value' => $model->getModePaymentText(),
            ],
            [
                'attribute' => 'Стоимость',
                'value' => $model->getCost(),
            ],
            'admin_comment:ntext',
            // 'user_comment:ntext',
        ],
    ]) ?>

</div>
