<?php

use yii\helpers\Html;
?>
    <h2 class="title_order_page">Ваши данные</h2>
<?php
$this->params['breadcrumbs'][] = ['label' => 'Orders', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="order-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>


<style type="text/css">
body{
    background-color: #eee;
}
</style>
