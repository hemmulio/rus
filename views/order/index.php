<?php

use yii\helpers\Html;
use yii\widgets\ListView;
use yii\grid\GridView;
use yii\data\ActiveDataProvider;
use app\models\Order;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Заказы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="order-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php $dataProvider = new ActiveDataProvider([
        'query' => Order::find(),
        'pagination' => [
            'pageSize' => 20,
        ],
    ]);
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'id',
            [
                'label' => 'Время',
                'attribute' => 'datatime',
                // 'format' => ['date', 'php:Y-m-d']
            ],
            // 'datatime',
            [
                'attribute' => 'status',
                'value' => function ($data) {
                    return Order::getStatus($data->status);
                },
            ],
            'name',
            'phone',
            'email',
            'address',
            [
                'attribute' => 'mode_delivery',
                'value' => function ($data) {
                    return Order::getMode_deliveryValue($data->mode_delivery);
                },
            ],
            [
                'attribute' => 'mode_payment',
                'value' => function ($data) {
                    return Order::getMode_paymentValue($data->mode_payment);
                },
            ],
            'admin_comment',
            [
                'attribute' => 'стоимость',
                'value' => function ($data) {
                    return $data->cost;
                },
            ],
            [
                'attribute' => 'заказ',
                'format' => 'html',
                'value' => function ($data) {
                    return $data->cargo;
                },
            ],


            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]);
    ?>




</div>

