<?php
use app\components\Yandex;
use app\models\Order;


?>
<!-- <form action="https://money.yandex.ru/eshop.xml" method="post">
<input name="shopId" value="<?= Yandex::SHOPID ?>" type="hidden"/>
<input name="scid" value="<?= Yandex::SCID ?>" type="hidden"/>
<input name="sum" value="<?= $order->cost ?>" type="hidden">
<input name="customerNumber" value="<?= $order->id ?>" type="hidden"/>
<input type="submit" value="Заплатить"/>
</form>
 -->


<form name=ShopForm method="POST" action="<?= Yandex::SEND_URL ?>">
	<input name="shopId" value="<?= Yandex::SHOPID ?>" type="hidden"/>
	<input name="scid" value="<?= Yandex::SCID ?>" type="hidden"/>
	<input name="sum" value="<?= $order->cost ?>" type="hidden">
	<input name="customerNumber" value="<?= $order->id ?>" type="hidden"/>
	<input name="paymentType" value="<?= Order::getPaymentModeText($order->mode_payment) ?>" type="hidden">

	<input type=submit value="Оплатить" style="display:none">
</form>

<script type="text/javascript">
window.onload = function(){
  document.forms['ShopForm'].submit()
}
</script>
