<?php

use yii\helpers\Html;
// use yii\widgets\ActiveForm;
use \kartik\widgets\ActiveForm;
// use kartik\form\ActiveField;
/* @var $this yii\web\View */
/* @var $model app\models\Order */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="order-form">

    <?php $form = ActiveForm::begin([
        'type' => ActiveForm::TYPE_HORIZONTAL,
        'formConfig' => ['labelSpan' => 3, 'deviceSize' => ActiveForm::SIZE_SMALL],
        // options => [class => 'col-sm-5'];
    ]); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => 255]) ?>
    <?= $form->field($model, 'email')->textInput(['maxlength' => 255]) ?>
    <?= $form->field($model, 'phone')->textInput(['maxlength' => 255]) ?>

    <hr>
    <?= $form->field($model, 'mode_delivery')->dropdownlist(
        [
            '0' => 'Доставка курьером',
            '1' => 'Самовывоз',
            '2' => 'Почтой России',
        ]
    ) ?>
    <?= $form->field($model, 'address')->textArea(['maxlength' => 255]) ?>

    <hr>

    <?= $form->field($model, 'mode_payment')->dropdownlist(
        $model::$mode_paymentAvailable
    ) ?>


    <div class="form-group">
        <?= Html::submitButton('Подтвердить', ['class' => 'btn btn-success pull-right']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
