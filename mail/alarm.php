<?php
use app\models\Order;
use app\models\Product;
/** @var Order $model */
?>
<p>
    <b>Поступил заказ</b><br>
    <p><b>Номер заказа <?= $model->id?></b></p>
</p>

<p>
    <b>Время:</b> <?= $model->datatime ?><br>
    <b>E-mail:</b> <?= $model->email ?><br>
    <b>Телефон:</b> <?= $model->phone ?><br>
    <b>Сбособ доставки:</b> <?= Order::$mode_deliveryAvailable[$model->mode_delivery] ?><br>
    <b>Адрес доставки:</b> <?= $model->address ?><br>
    <b>Способ оплаты:</b> <?= Order::$mode_paymentAvailable[$model->mode_payment] ?><br>
</p>

<p><b>Номер заказа <?= $model->id?></b></p>
<table>
    <tr>
        <th>Название</th>
        <th>Описание</th>
        <th>Тип</th>
        <th>Цвет</th>
        <th>Размер</th>
        <th>Количество</th>
        <th>Цена</th>
    </tr>

    <?php
    $all_total = 0;
    foreach ($model->carts as $key => $product) {
        $total_row = $product->count*$product->product->price;
        $all_total += $total_row;
    ?>
        <tr>
            <td><?= ($product->product->type == 0) ? 'Футболка' : 'Плакат'; ?></td>
            <td><?= $product->product->description ?></td>
            <td>
                <?php
                    if ($product->product->type == 0) {
                        echo $product->product->cutText;
                    } else {
                        $type = ($product->product->cut == 0) ? 'Без рамки' : 'С рамкой';
                        echo $type;
                    }
                ?>
            </td>
            <td>
                <?php
                    if ($product->product->type == 0) {
                        echo $product->product->colorRu;
                    } else {
                        echo Product::$posterColorRus[$product->product->color];
                    }
                ?>
            </td>
            <td><?= ($product->product->type == 0) ? $product->product->sizeText : ''; ?></td>
            <td><?= $product->count ?></td>
            <td><?= $total_row ?></td>
        </tr>
    <?php } ?>

    <tr>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td><b><?= $all_total ?></b></td>
    </tr>
</table>
