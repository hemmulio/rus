<?php

namespace app\components;

/**
* класс для работы с Яндекс.Кассой
*/
class Yandex
{
    const SHOPID = 27611;
    const SCID = 19085;
    const SEND_URL = 'https://money.yandex.ru/eshop.xml';

}
