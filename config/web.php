<?php

$params = require(__DIR__ . '/params.php');
// ini_set('sendmail_path', ' /usr/sbin/ssmtp -t -f')
$config = [
    'id' => 'basic',
    'language' => 'ru',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'components' => [
        'cart' => [
            'class' => 'yz\shoppingcart\ShoppingCart',
            'cartId' => 'my_application_cart',
        ],
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => '1111',
            // 'noCsrfRoutes' => [
            //     'yandex/check'
            // ]
        ],
        'urlManager' => [
            'class' => 'yii\web\UrlManager',
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules'=>[
                'check' => 'yandex/check',
                'checkdemo' => 'yandex/check',
                'faildemo' => 'yandex/fail',
                'fail' => 'yandex/fail',
                't-shirt/index' => 'cart/index',
                'tzars/index' => 'site/index',
                'tzars/all' => 'site/governments',
            ]
        ],
// checkURL https://tzars.ru/check
// avisoURL https://tzars.ru/aviso
// successURL https://tzars.ru/success
// successURL https://tzars.ru/fail

// Я хочу провести тестовые платежи
// checkURL https://tzars.ru/checkdemo
// avisoURL https://tzars.ru/avisodemo
// successURL https://tzars.ru/successdemo
// successURL https://tzars.ru/faildemo





        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // 'transport' => [
            //     'class' => 'Swift_SmtpTransport',
            //     'host' => 'smtp.yandex.ru',
            //     'username' => 'info@tzars.ru',
            //     'password' => 'tzars-2015',
            //     'port' => '465',
            //     'encryption' => 'tls',
            // ],
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            // 'useFileTransport' => true,
        ],

        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['info'],
                    'categories' => ['yandex'],
                    'logFile' => '@app/runtime/logs/yandex.log',
                    'maxFileSize' => 1024 * 2,
                    'maxLogFiles' => 20,
                ],
            ],
        ],
        'db' => require(__DIR__ . '/db.php'),
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = 'yii\debug\Module';

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = 'yii\gii\Module';
}

return $config;
